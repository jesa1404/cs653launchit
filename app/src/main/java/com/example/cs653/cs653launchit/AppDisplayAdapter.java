package com.example.cs653.cs653launchit;

import android.app.Activity;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.cs653.cs653launchit.R;

import java.util.ArrayList;

/**
 * Created by jeyasoorya on 3/3/18.
 */

public class AppDisplayAdapter extends ArrayAdapter<AppIconName> {

    public AppDisplayAdapter(Activity context){
        super(context,0);
    }
    @NonNull
    @Override

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){
        View gridItem = convertView;
        if(gridItem==null){
            gridItem = LayoutInflater.from(getContext()).inflate(R.layout.grid_item,parent,false);
        }
        AppIconName currApp = getItem(position);
        ImageView icon = gridItem.findViewById(R.id.appicon);
        icon.setImageDrawable(currApp.getAppIcon());
        TextView name = gridItem.findViewById(R.id.appname);
        name.setText(currApp.getAppLabel().toString());
        return gridItem;
    }
}
