package com.example.cs653.cs653launchit;

import android.graphics.drawable.Drawable;

/**
 * Created by jeyasoorya on 3/3/18.
 */

public class AppIconName {
    private Drawable appIcon;
    private CharSequence appLabel;
    private String pkgName;
    private long priority;

    public AppIconName(Drawable icon,CharSequence name,String pkg){
        appIcon = icon;
        appLabel = name;
        pkgName = pkg;
        priority = 0;
    }

    public AppIconName(Drawable icon,CharSequence name,String pkg,long p){
        appIcon = icon;
        appLabel = name;
        pkgName = pkg;
        priority = p;
    }

    public Drawable getAppIcon(){
        return appIcon;
    }

    public CharSequence getAppLabel(){
        return appLabel;
    }

    public String getPkgName(){
        return pkgName;
    }

    public long getPriority() { return priority; }

    public void setPriority(long p){
        priority = p;
    }
}
