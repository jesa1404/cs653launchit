package com.example.cs653.cs653launchit;

/**
 * Created by aadhavan on 13/4/18.
 */

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {
    private Context context;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    public  PreferenceManager(Context context){
        this.context=context;
        preferences = context.getSharedPreferences("LaunchIt",0);
        editor = preferences.edit();
    }
    public void setPreference(String preferenceName, String preferenceValue){
        editor.putString(preferenceName,preferenceValue);
        editor.commit();
    }
    public String getPreference(String preferenceName) {
        return preferences.getString(preferenceName,"None");
    }
}
