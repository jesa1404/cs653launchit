package com.example.cs653.cs653launchit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AlarmReceiver1 extends BroadcastReceiver {

    private Context context;
    PreferenceManager preferenceManager;
    public static final int REQUEST_CODE = 12346;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        preferenceManager = new PreferenceManager(context);
        try {
            if ( new refreshModel().execute(preferenceManager.getPreference("Training ID")).get())
                Toast.makeText(context,"ML model updated!",Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            Log.e("Error in ", "onItemClick: "+e.getMessage(),e );
        }
    }
    public class refreshModel extends AsyncTask<String,Integer,Boolean> {
        @Override
        protected Boolean doInBackground(String... args) {
            try {
                URL url = new URL("http://10.42.0.1:8000/getModel.php");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("file_id", args[0]);
                String query = builder.build().getEncodedQuery();
                DataOutputStream dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(query);
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);
                if(serverResponseCode == 200){
                    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    File directory = context.getExternalFilesDir(null);
                    File outfile = new File(directory, "Model.txt");
                    FileOutputStream fileOutputStream = new FileOutputStream(outfile,false);
                    String line = "";
                    while ((line = in.readLine()) != null) {
                        fileOutputStream.write(line.getBytes());
                        fileOutputStream.write("\n".getBytes());
                    }
                    fileOutputStream.close();
                    in.close();
                }
                dos.flush();
                dos.close();
                conn.disconnect();
            }
            catch (MalformedURLException ex) {
                Log.e("MalformedURLException", "error: " + ex.getMessage(), ex);
            }
            catch (Exception e) {
                Log.e("Others", "Exception : "
                        + e.getMessage(), e);
            }
            return true;
        }
    }
}
