package com.example.cs653.cs653launchit;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

public class UISettings extends AppCompatActivity {
    boolean setHomeLocation = false;
    boolean setWorkLocation = false;
    int PLACE_PICKER_REQUEST = 1;
    Activity cur;
    PreferenceManager p;
    AlarmManager alarmManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        cur=this;
        p = new PreferenceManager(this);
        setContentView(R.layout.activity_uisettings);
        ArrayList<String> required = new ArrayList<>();
        required.add("Get home location");
        required.add("Get work location");
        required.add("Refresh Data model");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,required);
        ListView l = findViewById(R.id.settings);
        l.setAdapter(adapter);
        l.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("selected",Long.toString(l));
                if(l==0){
                    setHomeLocation = true;
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(cur), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                }
                else if(l==1){
                    setWorkLocation = true;
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    try {
                        startActivityForResult(builder.build(cur), PLACE_PICKER_REQUEST);
                    } catch (GooglePlayServicesRepairableException e) {
                        e.printStackTrace();
                    } catch (GooglePlayServicesNotAvailableException e) {
                        e.printStackTrace();
                    }
                }
                else if(l==2){
                    try {
                        if (new refreshData().execute().get())
                            Toast.makeText(cur, "Uploaded available data. ML model will be updated shortly", Toast.LENGTH_LONG).show();
                    }
                    catch (Exception e){
                        Log.e("Error in ", "onItemClick: "+e.getMessage(),e );
                    }
                }
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this,data);
                if(setHomeLocation){
                    setHomeLocation=false;
                    p.setPreference("homeLocLat",Double.toString(place.getLatLng().latitude));
                    p.setPreference("homeLocLng",Double.toString(place.getLatLng().longitude));
                }
                if(setWorkLocation){
                    setWorkLocation=false;
                    p.setPreference("workLocLat",Double.toString(place.getLatLng().latitude));
                    p.setPreference("workLocLng",Double.toString(place.getLatLng().longitude));
                }
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }
    }

    public class refreshData extends AsyncTask<String,Integer,Boolean> {
        @Override
        protected Boolean doInBackground(String... args) {
            if(isNetworkAvailable()){
                HttpURLConnection conn = null;
                DataOutputStream dos = null;
                String lineEnd = "\r\n";
                String twoHyphens = "--";
                String boundary = "*****";
                int bytesRead, bytesAvailable, bufferSize;
                byte[] buffer;
                int maxBufferSize = 1 * 1024 * 1024;
                File directory = getExternalFilesDir(null);
                File sourceFile = new File(directory, "Data.csv");
                if (!sourceFile.isFile()) {
                    return false;
                } else {
                    try {
                        FileInputStream fileInputStream = new FileInputStream(sourceFile);
                        URL url = new URL("http://10.42.0.1:8000/postData.php");
                        conn = (HttpURLConnection) url.openConnection();
                        conn.setDoInput(true); // Allow Inputs
                        conn.setDoOutput(true); // Allow Outputs
                        conn.setUseCaches(false); // Don't use a Cached Copy
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Connection", "Keep-Alive");
                        conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                        conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                        conn.setRequestProperty("uploaded_file", "Data.csv");
                        dos = new DataOutputStream(conn.getOutputStream());
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=data;filename=Data.csv" + lineEnd);
                        dos.writeBytes(lineEnd);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        buffer = new byte[bufferSize];
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        while (bytesRead > 0) {
                            dos.write(buffer, 0, bufferSize);
                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                        }
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                        int serverResponseCode = conn.getResponseCode();
                        if (serverResponseCode == 200) {
                            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            p.setPreference("Training ID", in.readLine());
                            in.close();
                            Intent intent = new Intent(getApplicationContext(), AlarmReceiver1.class);
                            intent.putExtra("Action", "RefreshModel");
                            final PendingIntent pIntent = PendingIntent.getBroadcast(cur, AlarmReceiver1.REQUEST_CODE,
                                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+120000, pIntent);
                        }
                        fileInputStream.close();
                        dos.flush();
                        dos.close();
                        conn.disconnect();
                    } catch (MalformedURLException ex) {
                        Log.e("MalformedURLException", "error: " + ex.getMessage(), ex);
                    } catch (Exception e) {
                        Log.e("Others", "Exception : "
                                + e.getMessage(), e);
                    }
                    return true;
                }
            }
            else {
                Toast.makeText(cur,"Please enable internet and try again",Toast.LENGTH_LONG).show();
                return false;
            }
        }
        private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
    }
}
