package com.example.cs653.cs653launchit;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * Created by aadhavan on 4/3/18.
 */

public class AppListLoader extends AsyncTaskLoader<ArrayList<AppIconName>> {
    ArrayList<AppIconName> loadedList;
    PackageManager packageManager;
    PackageChangeListener packageChangeListener;
    UsageStatsManager usageStatsManager;

    public AppListLoader(Context context){
        super(context);
        packageManager = context.getPackageManager();
        usageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
    }

    @Override
    public ArrayList<AppIconName> loadInBackground() {
        List<ApplicationInfo> apps = packageManager.getInstalledApplications(0);
        ArrayList<AppIconName> appList = new ArrayList<>();
        if (apps == null) {
            apps = new ArrayList<>();
        }
        for (int i = 0; i < apps.size(); i++) {
            String pkg = apps.get(i).packageName;
            if (packageManager.getLaunchIntentForPackage(pkg) != null) {
                CharSequence label = (apps.get(i).loadLabel(packageManager));
                Drawable icon = packageManager.getApplicationIcon(apps.get(i));
                appList.add(new AppIconName(icon,label,pkg));
            }
        }
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.YEAR,-1);
//        Map<String,UsageStats> queryUsageStats1 = usageStatsManager.queryAndAggregateUsageStats(cal.getTimeInMillis(),
//                System.currentTimeMillis());
//
//        if(queryUsageStats1==null){
//            Log.v("Hello","h");
//        }
//        else {
//            for (int j = 0; j < appList.size(); j++) {
//                try {
//                    appList.get(j).setPriority(queryUsageStats1.get(appList.get(j).getPkgName()).getLastTimeUsed());
//                    if (appList.get(j).getPkgName().equals("com.example.cs653.cs653launchit")) {
//                        appList.get(j).setPriority(-1);
//                    }
//                }
//                catch (Exception e){
////                    e.printStackTrace();
//                }
//            }
//        }

        Collections.sort(appList,MyComparator);
        loadedList = appList;
        return appList;
    }

    @Override
    public void deliverResult(ArrayList<AppIconName> data) {

        super.deliverResult(data);
    }

    @Override
    protected void onStartLoading() {
        if(loadedList!=null)
            super.deliverResult(loadedList);
        if(packageChangeListener==null)
            packageChangeListener=new PackageChangeListener(this);
        if(takeContentChanged()||loadedList==null)
            forceLoad();
    }

    @Override
    protected void onReset() {
        cancelLoad();
        if(loadedList!=null)
            loadedList=null;
        if(packageChangeListener!=null){
            getContext().unregisterReceiver(packageChangeListener);
            packageChangeListener=null;
        }
    }

    public static final Comparator<AppIconName> MyComparator = new Comparator<AppIconName>() {
        @Override
        public int compare(AppIconName object1, AppIconName object2) {
            return object1.getAppLabel().toString().compareTo(object2.getAppLabel().toString());
        }
    };

    public static final Comparator<AppIconName> MyComparator1 = new Comparator<AppIconName>() {
        @Override
        public int compare(AppIconName object1, AppIconName object2) {
            if(object1.getPriority()<object2.getPriority()){
                return 1;
            }
            if(object1.getPriority()>object2.getPriority()){
                return -1;
            }
            return 0;
        }
    };


}
