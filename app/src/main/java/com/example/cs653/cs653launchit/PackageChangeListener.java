package com.example.cs653.cs653launchit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

/**
 * Created by aadhavan on 4/3/18.
 */

public class PackageChangeListener extends BroadcastReceiver {
    final AppListLoader appListLoader;
    public PackageChangeListener(AppListLoader loader){
        appListLoader = loader;
        IntentFilter filter = new IntentFilter(Intent.ACTION_PACKAGE_ADDED);
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED);
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED);
        filter.addDataScheme("package");
        appListLoader.getContext().registerReceiver(this, filter);
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("here");
        appListLoader.onContentChanged();
    }
}
