package com.example.cs653.cs653launchit;

import android.app.AppOpsManager;
import android.app.IntentService;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Intent;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

public class UsageStatsCollector extends IntentService {

    private File directory;
    private File outfile;
    private File readfile;
    private FileOutputStream fileOutputStream;
    private PackageManager packageManager;
    private LocationManager locationManager;
    private PreferenceManager preferenceManager;

    public UsageStatsCollector() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Calendar cal = Calendar.getInstance();
        preferenceManager = new PreferenceManager(this);
        packageManager = getPackageManager();
        locationManager =  (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        directory = getExternalFilesDir(null);
        outfile = new File(directory, "Data.csv");
        readfile = new File(directory,"Model.txt");
        List<ApplicationInfo> apps = packageManager.getInstalledApplications(0);
        ArrayList<String> appList = new ArrayList<>();
        if (apps == null) {
            apps = new ArrayList<>();
        }
        for (int i = 0; i < apps.size(); i++) {
            String pkg = apps.get(i).packageName;
            if (packageManager.getLaunchIntentForPackage(pkg) != null) {
                appList.add(pkg);
            }
        }
        Collections.sort(appList);
        try {
            System.out.println("DataCollected");
            fileOutputStream = new FileOutputStream(outfile,true);
            ArrayList<Double> X = new ArrayList<>();
            for(int ind=0;ind<10;ind++){
                X.add(0.0);
            }
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            UsageStatsManager usageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            cal.add(Calendar.MINUTE, -15);
            Long currentTime = System.currentTimeMillis();
            List<UsageStats> queryUsageStats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, cal.getTimeInMillis(),
                    currentTime);
            cal.add(Calendar.MINUTE,15);
            cal.add(Calendar.YEAR,-1);
            List<UsageStats> queryUsageStats1 = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_YEARLY, cal.getTimeInMillis(),
                    System.currentTimeMillis());
            Date currd = new Date();
            String data = new SimpleDateFormat("HH:mm,E").format(currd)+",";
            X.add(0,Double.valueOf(currd.getHours()*4+currd.getMinutes()/15));
            X.add(currd.getDay(),Double.valueOf(1));
            Location location1 = new Location(LocationManager.NETWORK_PROVIDER);
            if(preferenceManager.getPreference("homeLocLat").equals("None")) {
                data = data + "-1,";
                Toast.makeText(this,"Please set your home Location",Toast.LENGTH_LONG).show();
            }
            else {
                location1.setLatitude(Double.valueOf(preferenceManager.getPreference("homeLocLat")));
                location1.setLongitude(Double.valueOf(preferenceManager.getPreference("homeLocLng")));
                float d1;
                d1 = location1.distanceTo(location);
                if(d1<100){
                    X.add(8,1.0);
                }
                data = data+location1.distanceTo(location)+",";
            }
            if(preferenceManager.getPreference("workLocLat").equals("None")) {
                data = data + "-1";
                Toast.makeText(this,"Please set your work Location",Toast.LENGTH_LONG).show();
            }
            else {
                location1.setLatitude(Double.valueOf(preferenceManager.getPreference("workLocLat")));
                location1.setLongitude(Double.valueOf(preferenceManager.getPreference("workLocLng")));
                float d2;
                d2 = location1.distanceTo(location);
                if(d2<100){
                    X.add(9,1.0);
                }
                data = data+location1.distanceTo(location);
            }
            for (int i = 0; i < appList.size(); i++) {
                data = data + "," + appList.get(i) + ",";
                int j=0;
                for (;j<queryUsageStats.size();j++)
                    if(appList.get(i).equals(queryUsageStats.get(j).getPackageName())) {
                        break;
                    }
                if(j<queryUsageStats.size()) {
//                    String lastTime = Long.toString(currentTime-Long.valueOf(queryUsageStats.get(j).getLastTimeUsed()));
//                    data = data + lastTime + ",";
                    data = data + queryUsageStats.get(j).getLastTimeUsed() + ",";
                    data = data + queryUsageStats.get(j).getTotalTimeInForeground();
                }
                else{
                    int k=0;
                    for (;k<queryUsageStats1.size();k++)
                        if(appList.get(i).equals(queryUsageStats1.get(k).getPackageName())) {
                            break;
                        }
                    if(k<queryUsageStats1.size()) {
//                        String lastTime = Long.toString(currentTime-Long.valueOf(queryUsageStats1.get(k).getLastTimeUsed()));
//                        data = data + lastTime + ",";
                        data = data + queryUsageStats1.get(k).getLastTimeUsed() + ",";
                        data = data + "0";
                    }
                    else{
                        data = data + "0,0";
                    }
                }
            }
            data = data + "\n";
            fileOutputStream.write(data.getBytes());
            fileOutputStream.close();
            if(readfile.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(readfile));
                int coeff = 0;
                String line;
                ArrayList<ArrayList<Double>> coeff1,coeff2;
                coeff1 = new ArrayList<>();
                coeff2 = new ArrayList<>();
                while((line = br.readLine())!=null){
                    if(line.equals("")){
                        continue;
                    }
                    if(line.equals("Layer")){
                        coeff += 1;
                        continue;
                    }
                    String[] s = line.split(" ");
                    ArrayList<Double> temp = new ArrayList<>();
                    for(int i=0;i<s.length;i++){
                        temp.add(Double.valueOf(s[i]));
                    }
                    if(coeff==1){
                        coeff1.add(temp);
                    }
                    else if(coeff==2){
                        coeff2.add(temp);
                    }
                }
                NeuralNet N = new NeuralNet(coeff1,coeff2);
                ArrayList<Integer> predicted = N.predict(X);
                for(int ind=0;ind<predicted.size();ind++) {
                    preferenceManager.setPreference("App" + Integer.toString(ind + 1), appList.get(predicted.get(ind)));
                }
            }

        }
        catch (SecurityException e){
            Log.e("DataCollection:", "Error while collecting app usage statistics", e);
        }
        catch (Exception e) {
            Log.e("DataCollection:", "Error while collecting app usage statistics", e);
        }
    }
}
