package com.example.cs653.cs653launchit;

import android.Manifest;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.LoaderManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;


public class AppList extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<AppIconName>> {

    AppDisplayAdapter adapter;
    AppDisplayAdapter adapter1;
    ArrayList<AppIconName> currList;
    EditText searchApp;
    ProgressBar p;
    GridView appGrid1;
    GridView appGrid;
    AppListLoader loadApps;
    private PreferenceManager preferenceManager;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startDataCollect();
        setContentView(R.layout.activity_app_list);
        appGrid1 = findViewById(R.id.listApps1);
        appGrid = findViewById(R.id.listApps);
        searchApp = findViewById(R.id.searchApp);
        p = findViewById(R.id.progressBar2);
        adapter = new AppDisplayAdapter(this);
        adapter1 = new AppDisplayAdapter(this);
        appGrid1.setAdapter(adapter1);
        appGrid.setAdapter(adapter);
        appGrid1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AppIconName selectedApp = adapter.getItem(i);
                Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(selectedApp.getPkgName());
                if (intent != null) {
                    startActivity(intent);
                }
                else {
                    Log.e("App launch Error", selectedApp.getPkgName());
                }
            }
        });
        appGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AppIconName selectedApp = adapter.getItem(i);
                Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(selectedApp.getPkgName());
                if (intent != null) {
                    startActivity(intent);
                }
                else {
                    Log.e("App launch Error", selectedApp.getPkgName());
                }
            }
        });
        appGrid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                AppIconName selectedApp = adapter.getItem(i);
                UninstallAlert ua = new UninstallAlert();
                ua.setPackageName(selectedApp.getPkgName());
                ua.show(getFragmentManager(),"Alert Dialog");
                return false;
            }
        });
        searchApp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(currList==null){
                    Log.d("Error","currlist null");
                }
                if(charSequence.length()==0){
                    adapter.clear();
                    adapter.addAll(currList);
                    return;
                }
                Log.v("ChangedText",charSequence.toString());
                ArrayList<AppIconName> temp = new ArrayList<>();
                for(int k=0;k<currList.size();k++){
                    CharSequence appLabel = currList.get(k).getAppLabel();
                    for(int l=0,m=i;;){
                        if(m>=i2){
                            temp.add(currList.get(k));
                            break;
                        }
                        else if(l>=appLabel.length()&&m<i2){
                            break;
                        }
                        else if(l<appLabel.length()&&m<i2){
                            if(Character.toLowerCase(appLabel.charAt(l))==Character.toLowerCase(charSequence.charAt(m))){
                                l++;
                                m++;
                            }
                            else{
                                break;
                            }
                        }
                    }
                }
                adapter.clear();
                adapter.addAll(temp);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        loadApps=(AppListLoader) getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onResume(){
        super.onResume();
        appGrid1.setVisibility(View.GONE);
        appGrid.setVisibility(View.GONE);
        p.setVisibility(View.VISIBLE);
        loadApps.forceLoad();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public Loader<ArrayList<AppIconName>> onCreateLoader(int i, Bundle bundle) {
        return new AppListLoader(this);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<AppIconName>> loader) {
        adapter.clear();
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<AppIconName>> loader, ArrayList<AppIconName> appIconNames) {
        currList = new ArrayList<>(appIconNames);
        p.setVisibility(View.GONE);
        appGrid1.setVisibility(View.VISIBLE);
        appGrid.setVisibility(View.VISIBLE);
        adapter.clear();
        adapter1.clear();
        if(!preferenceManager.getPreference("App1").equals("None")){
            PackageManager pm = getPackageManager();
            for(int i=0;i<4;i++){
                try {
                    System.out.println(preferenceManager.getPreference("App"+Integer.toString(i+1)));
                    ApplicationInfo a = pm.getApplicationInfo(preferenceManager.getPreference("App"+Integer.toString(i+1)),0);
                    CharSequence label = (a.loadLabel(pm));
                    Drawable icon = pm.getApplicationIcon(a);
                    adapter1.add(new AppIconName(icon,label,preferenceManager.getPreference("App"+Integer.toString(i+1))));
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            for (int i = 0; i < 4; i++) {
                adapter1.add(appIconNames.get(i));
            }
        }
        adapter.addAll(appIconNames);
    }
    private boolean hasUsageStatsPermission() {
        AppOpsManager appOps = (AppOpsManager)
                getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), getPackageName());
        return mode == AppOpsManager.MODE_ALLOWED;
    }

    public void startDataCollect() {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        preferenceManager = new PreferenceManager(this);
        if(!hasUsageStatsPermission() && !preferenceManager.getPreference("ACTION_USAGE_ACCESS_SETTINGS_Permission_requested").equals("True")){
            startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
            preferenceManager.setPreference("ACTION_USAGE_ACCESS_SETTINGS_Permission_requested","True");
        }
        if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !preferenceManager.getPreference("Enable_GPS_requested").equals("True")) {
            Toast.makeText(this,"Please Enable GPS", Toast.LENGTH_LONG).show();
            preferenceManager.setPreference("Enable_GPS_requested","True");
        }
        else {
            if (!hasLocationPermission() && !preferenceManager.getPreference("ACCESS_LOCATION_Permission_requested").equals("True")) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                preferenceManager.setPreference("ACCESS_LOCATION_Permission_requested","True");
            }
        }
        boolean alarmUp = (PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE,
                new Intent(getApplicationContext(), AlarmReceiver.class),
                PendingIntent.FLAG_NO_CREATE) != null);
        if(hasLocationPermission() && hasUsageStatsPermission() && !alarmUp) {
            System.out.println("AlarmSet");
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            final PendingIntent pIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            long firstMillis = System.currentTimeMillis();
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis,
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES, pIntent);
        }
    }

    public boolean hasLocationPermission()
    {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }
}
