package com.example.cs653.cs653launchit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class NeuralNet {
    private ArrayList<ArrayList<Double>> coeff1,coeff2;

    public NeuralNet(ArrayList<ArrayList<Double>> c1,ArrayList<ArrayList<Double>> c2){
        coeff1 = c1;
        coeff2 = c2;
    }

    public static ArrayList<Double> multiply(ArrayList<ArrayList<Double>> M,ArrayList<Double> V){
        int rows = M.size(),cols = M.get(0).size();
        ArrayList<Double> res = new ArrayList<>();
        for(int j=0;j<cols;j++){
            Double sum = 0.0;
            for(int i=0;i<rows;i++){
                sum = sum + M.get(i).get(j)*V.get(i);
            }
            res.add(sum);
        }
        return res;
    }

    public static ArrayList<Double> sigmoid(ArrayList<Double> V){
        ArrayList<Double> res = new ArrayList<>();
        for(int i=0;i<V.size();i++){
            Double x = 1/(1+Math.exp(-V.get(i)));
            res.add(x);
        }
        return res;
    }

    public ArrayList<Integer> predict(ArrayList<Double> x){
        ArrayList<Double> y = sigmoid(multiply(coeff2,sigmoid(multiply(coeff1,x))));
        ArrayList<Prediction> p = new ArrayList<>();
        for(int i=0;i<y.size();i++){
            p.add(new Prediction(i,y.get(i)));
        }
        Collections.sort(p,new MyComp());
        ArrayList<Integer> res = new ArrayList<>();
        for(int i=0;i<4;i++){
            res.add(p.get(i).index);
        }
        return res;
    }

    private class Prediction{
        public int index;
        public Double val;
        public Prediction(int i,Double v){
            index = i;
            val = v;
        }
    }

    private class MyComp implements Comparator<Prediction>{
        @Override
        public int compare(Prediction object1, Prediction object2) {
            double d = object1.val-object2.val;
            if(d>0)
                return 1;
            else if(d<0)
                return -1;
            return 0;
        }
    }
}
